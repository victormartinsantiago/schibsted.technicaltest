﻿namespace Schibsted.TechnicalTest.Tests.Common
{
    using System;
    using System.IO;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public abstract class TestBase
    {
        private IConfigurationBuilder _configurationBuilder;
        private IServiceCollection _serviceCollection;

        public TestBase()
        {
            _configurationBuilder = new ConfigurationBuilder();
            _configurationBuilder.SetBasePath(Path.Combine(AppContext.BaseDirectory));

            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddOptions();

            AddConfiguration(_configurationBuilder);
            RegisterDependencies(_serviceCollection);
        }

        protected IConfiguration Configuration => _configurationBuilder.Build();

        protected IServiceProvider ServiceProvider => _serviceCollection.BuildServiceProvider();

        public abstract void AddConfiguration(IConfigurationBuilder configurationBuilder);

        public abstract void RegisterDependencies(IServiceCollection serviceCollection);
    }
}
