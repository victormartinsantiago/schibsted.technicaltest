﻿namespace Schibsted.TechnicalTest.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Schibsted.TechnicalTest.Common.Model;
    using Schibsted.TechnicalTest.Common.Requests;
    using Schibsted.TechnicalTest.Core.Abstractions;

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;

        public UserController(
            IUserService userService,
            ILogger<UserController> logger)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> Get()
        {
            _logger.LogInformation($"Trying to fetch all users");
            return new ActionResult<IEnumerable<User>>(await _userService.GetAll());
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<User>> Get(Guid userId)
        {
            _logger.LogInformation($"Trying to fetch user with ID {userId}");
            return new ActionResult<User>(await _userService.GetByIdAsync(userId));
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Post([FromBody]AddUserRequest request)
        {
            _logger.LogInformation($"Adding new user");
            return new ActionResult<Guid>(await _userService.CreateAsync(request.Map()));
        }

        [HttpPut]
        public async Task<ActionResult> Put([FromBody]User user)
        {
            _logger.LogInformation($"Updating user {user}");
            await _userService.UpdateAsync(user);

            return Ok();
        }

        [HttpDelete("{userId}")]
        public async Task<ActionResult> Delete(Guid userId)
        {
            _logger.LogInformation($"Deleting user with ID {userId}");
            await _userService.DeleteAsync(userId);

            return Ok();
        }
    }
}
