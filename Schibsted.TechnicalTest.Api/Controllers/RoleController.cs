﻿namespace Schibsted.TechnicalTest.Api.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Schibsted.TechnicalTest.Common.Model;
    using Schibsted.TechnicalTest.Common.Requests;
    using Schibsted.TechnicalTest.Core.Abstractions;

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RoleController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;

        public RoleController(
            IUserService userService,
            ILogger<UserController> logger)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody]AddRolesRequest request)
        {
            _logger.LogInformation($"Adding roles for user {request.UserId}");
            await _userService.AddRoleAsync(request.UserId, request.Roles);

            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> Delete([FromBody]RemoveRoleRequest request)
        {
            _logger.LogInformation($"Deleting forles from user with ID {request.UserId}");
            await _userService.RemoveRoleAsync(request.UserId, request.Roles);

            return Ok();
        }
    }
}
