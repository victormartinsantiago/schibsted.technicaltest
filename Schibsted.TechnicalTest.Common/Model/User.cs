﻿namespace Schibsted.TechnicalTest.Common.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;
    using Schibsted.TechnicalTest.Common.Attributes;

    public class User
    {
        [NotEmpty]
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }

        public IEnumerable<RoleType> Roles { get; set; } = new List<RoleType>();

        public override string ToString()
        {
            return $"User {Id} - Username";
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is User))
            {
                return false;
            }

            var user = obj as User;

            return
                Id.Equals(user.Id) &&
                DisplayName.Equals(user.DisplayName) &&
                Username.Equals(user.Username) &&
                Password.Equals(user.Password);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
