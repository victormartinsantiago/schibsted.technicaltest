﻿namespace Schibsted.TechnicalTest.Common.Requests
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Schibsted.TechnicalTest.Common.Attributes;
    using Schibsted.TechnicalTest.Common.Model;

    public class AddRolesRequest
    {
        [NotEmpty]
        public Guid UserId { get; set; }

        [Required]
        [NotNullOrEmptyCollection]
        public IEnumerable<RoleType> Roles { get; set; }
    }
}
