﻿namespace Schibsted.TechnicalTest.Common.Requests
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Schibsted.TechnicalTest.Common.Model;

    public class AddUserRequest
    {
        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public IEnumerable<RoleType> Roles { get; set; } = new List<RoleType>();

        public User Map() => new User
        {
            DisplayName = DisplayName,
            Username = Username,
            Password = Password,
            Roles = Roles
        };
    }
}
