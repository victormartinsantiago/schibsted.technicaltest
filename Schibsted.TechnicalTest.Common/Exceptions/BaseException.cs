﻿namespace Schibsted.TechnicalTest.Common.Exceptions
{
    using System;

    public abstract class BaseException : Exception
    {
        public BaseException(string message)
            : base(message)
        {
        }

        public BaseException(string message, Exception exception)
          : base(message, exception)
        {
        }

        public abstract string Title { get; }

        public abstract int StatusCode { get; }
    }
}
