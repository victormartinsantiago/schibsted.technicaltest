﻿namespace Schibsted.TechnicalTest.Common.Attributes
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;

    public class NotEmptyAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "'{0}' guid cannot be empty";

        public NotEmptyAttribute()
            : base(DefaultErrorMessage)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var input = Convert.ToString(value, CultureInfo.CurrentCulture);

            if (string.IsNullOrWhiteSpace(input))
            {
                return null;
            }

            if (!Guid.TryParse(input, out var guid))
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            if (guid.Equals(Guid.Empty))
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            return null;
        }
    }
}
