﻿namespace Schibsted.TechnicalTest.Repository.Configurators
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Newtonsoft.Json;
    using Schibsted.TechnicalTest.Repository.Abstractions;
    using Schibsted.TechnicalTest.Repository.Models;
    using Schibsted.TechnicalTest.Repository.Repositories;

    public static class InversionOfControlConfigurator
    {
        private const string DatabaseName = "UsersDatabase";
        private const string DefaultPassword = "Pa$$w0rd";
        private const string RolesBasePath = @"Json\Roles.json";
        private const string UsersBasePath = @"Json\Users.json";

        public static void AddRepository(this IServiceCollection serviceCollection, bool loadDummies = true)
        {
            var userRepository = new UserRepository(CreateOptions<UserRepository>(loadDummies));
            serviceCollection.AddSingleton<IUserRepository>(userRepository);

            serviceCollection
                .BuildServiceProvider()
                .AddDummies(loadDummies);
        }

        private static DbContextOptions<T> CreateOptions<T>(bool loadDummies = true)
            where T : DbContext
        {
            var databaseName = loadDummies ? DatabaseName : $"Database-{Guid.NewGuid()}";
            return new DbContextOptionsBuilder<T>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        private static void AddDummies(this IServiceProvider serviceProvider, bool loadDummies)
        {
            var repository = serviceProvider.GetRequiredService<IUserRepository>();
            var roles = Load<Role>(RolesBasePath, loadDummies);
            var users = Load<User>(UsersBasePath, loadDummies);

            foreach (var role in roles)
            {
                repository.Roles.Add(role);
            }

            if (!loadDummies)
            {
                repository.SaveChanges();
                return;
            }

            var roleIndex = 0;
            foreach (var user in users)
            {
                var role = roles.ElementAt(roleIndex);
                user.SetPassword(DefaultPassword);
                user.Roles.Add(new UserRole
                {
                    UserId = user.Id,
                    RoleId = role.Id,
                    User = user,
                    Role = role
                });
                repository.Users.Add(user);

                roleIndex++;
            }

            repository.SaveChanges();
        }

        private static IEnumerable<T> Load<T>(string jsonFilePath, bool loadDummies)
        {
            var executingAssemblyPath = loadDummies
                ? Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)
                : Directory.GetCurrentDirectory();

            var filePath = Path.Combine(
                executingAssemblyPath,
                jsonFilePath);

            if (!File.Exists(filePath))
            {
                return Enumerable.Empty<T>();
            }

            try
            {
                var fileContent = File.ReadAllText(filePath);
                return JsonConvert.DeserializeObject<IEnumerable<T>>(fileContent);
            }
            catch (Exception)
            {
                return Enumerable.Empty<T>();
            }
        }
    }
}
