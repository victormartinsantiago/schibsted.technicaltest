﻿namespace Schibsted.TechnicalTest.Repository.Repositories
{
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Schibsted.TechnicalTest.Repository.Abstractions;
    using Schibsted.TechnicalTest.Repository.Models;

    public class UserRepository : DbContext, IUserRepository
    {
        public UserRepository(DbContextOptions<UserRepository> options)
            : base(options)
        {
        }

        public virtual DbSet<User> Users
        {
            get;
            set;
        }

        public virtual DbSet<Role> Roles
        {
            get;
            set;
        }

        public async Task SaveChangesAsync()
        {
            await base.SaveChangesAsync();
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserRole>()
                .HasKey(ur => new { ur.UserId, ur.RoleId });
        }
    }
}
