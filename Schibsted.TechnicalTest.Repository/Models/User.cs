﻿namespace Schibsted.TechnicalTest.Repository.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    [Table("User")]
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        [Key]
        public int Identity { get; set; }

        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public byte[] PasswordHash { get; set; }

        [Required]
        public Guid Salt { get; set; }

        [Required]
        public virtual ICollection<UserRole> Roles { get; set; } = new List<UserRole>();

        public bool CheckPassword(string password)
        {
            var passwordHash = GetPasswordHash(password, Salt);

            return passwordHash.SequenceEqual(PasswordHash);
        }

        public void SetPassword(string password)
        {
            PasswordHash = GetPasswordHash(password, Salt);
        }

        private byte[] GetPasswordHash(string password, Guid salt)
        {
            var value = Encoding.ASCII.GetBytes(password);
            var saltedValue = value.Concat(salt.ToByteArray()).ToArray();

            return new SHA256Managed().ComputeHash(saltedValue);
        }
    }
}
