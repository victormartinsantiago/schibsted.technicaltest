﻿namespace Schibsted.TechnicalTest.Repository.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Schibsted.TechnicalTest.Common.Model;

    [Table("Role")]
    public class Role
    {
        [Key]
        public Guid Id { get; set; }

        public RoleType RoleType { get; set; }

        public virtual ICollection<UserRole> Users { get; set; }
    }
}
