﻿namespace Schibsted.TechnicalTest.Repository.Abstractions
{
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Schibsted.TechnicalTest.Repository.Models;

    public interface IUserRepository
    {
        DbSet<User> Users { get; set; }

        DbSet<Role> Roles { get; set; }

        void SaveChanges();

        Task SaveChangesAsync();
    }
}
