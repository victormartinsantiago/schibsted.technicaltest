# Readme

## 1. Solution Structure

The solution is comprised of two webapi projects:

* WebSite: Provides the front-end component of the application. I used ```ASP.NET MVC``` instead of an SPA framework for simplicity.
* Api: Exposes the API resources to manage users

I belive the separation is important so that both components can be deployed independently while the core is still unique and reusable.

## 2. Choice of database

In order to facilitate development and make the process of running the solution easier, I chose an in memory implementation of SqlServer: ```SqlServer Microsoft.EntityFrameworkCore.InMemory```
As each project starts and runs independently, it means each project uses its own in memory database. In a real production scenario, however,
this would be replaced by a full sql server database, so both services would target the same database.

In other words, users created via the API are not visible by the WebSite project and viceversa. While this is an inconvenient to test, I believe
it is more important for me to show the clear separation of concerns.

To ease the testing process, I load a set of default dummy data when each service starts up. Json files can be found at ```Schibsted.TechnicalTest.Repository/Json```


## 3. Running the solution

### 3.1 Visual Studio

If using visual studio to run the solution, both the ```Schibsted.TechnicalTest.WebSite``` and ```Schibsted.TechnicalTest.Api``` projects must be set as startup projects.
After hitting run, each service should have started on IIS express and they would be listen on the below ports:

* Schibsted.TechnicalTest.Api: http://localhost:64745, https://localhost:44355
* Schibsted.TechnicalTest.WebSite: http://localhost:53216, https://localhost:44391

### 3.2 dotnet 

If using dotnet to build and run the solution, open a powershell or cmd and run the below commands from the solution's directory.

```ps1
cd /Schibsted.TechnicalTest
```

```ps1
dotnet restore
```

```ps1
dotnet build
```

```ps1
cd /Schibsted.TechnicalTest.Api
```

```ps1
dotnet run
```

```ps1
cd /Schibsted.TechnicalTest.WebSite
```

```ps1
dotnet run
```

Both services should have started up:

* Schibsted.TechnicalTest.Api: https://localhost:5001;http://localhost:5000
* Schibsted.TechnicalTest.WebSite: https://localhost:5002;http://localhost:5003