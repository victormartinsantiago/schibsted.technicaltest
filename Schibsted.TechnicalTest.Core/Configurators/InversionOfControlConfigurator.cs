﻿namespace Schibsted.TechnicalTest.Core.Configurators
{
    using Microsoft.Extensions.DependencyInjection;
    using Schibsted.TechnicalTest.Core.Abstractions;
    using Schibsted.TechnicalTest.Core.Services;
    using Schibsted.TechnicalTest.Repository.Configurators;

    public static class InversionOfControlConfigurator
    {
        public static void AddCoreDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddRepository();
            serviceCollection.AddTransient<IAuthenticationService, AuthenticationService>();
            serviceCollection.AddTransient<IUserService, UserService>();
        }
    }
}
