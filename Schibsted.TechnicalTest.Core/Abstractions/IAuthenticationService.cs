﻿namespace Schibsted.TechnicalTest.Core.Abstractions
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;

    public interface IAuthenticationService
    {
        Task<bool> LoginUserAsync(string username, string password, HttpContext httpContext);

        Task<ClaimsIdentity> AuthenticateApiUserAsync(HttpContext httpContext, string schemeName);
    }
}
