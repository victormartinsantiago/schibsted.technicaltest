﻿namespace Schibsted.TechnicalTest.Core.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Schibsted.TechnicalTest.Common.Model;

    public interface IUserService
    {
        Task<IEnumerable<User>> GetAll();

        Task<User> GetByIdAsync(Guid userId);

        Task<Guid> CreateAsync(User user);

        Task UpdateAsync(User user);

        Task DeleteAsync(Guid userId);

        Task DeleteAllAsync();

        Task AddRoleAsync(Guid userId, IEnumerable<RoleType> roles);

        Task RemoveRoleAsync(Guid userId, IEnumerable<RoleType> roles);
    }
}
