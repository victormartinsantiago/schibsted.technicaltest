﻿namespace Schibsted.TechnicalTest.Core.Services
{
    using System;
    using System.Linq;
    using System.Net.Http.Headers;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Schibsted.TechnicalTest.Common.Exceptions;
    using Schibsted.TechnicalTest.Common.Model;
    using Schibsted.TechnicalTest.Infrastructure.Extensions;
    using Schibsted.TechnicalTest.Repository.Abstractions;
    using Schibsted.TechnicalTest.Repository.Models;

    public class AuthenticationService : Abstractions.IAuthenticationService
    {
        private const string AuthorizationHeader = "Authorization";

        private readonly IUserRepository _userRepository;
        private readonly ILogger<AuthenticationService> _logger;

        public AuthenticationService(
            IUserRepository userRepository,
            ILogger<AuthenticationService> logger)
        {
            _logger = logger;
            _userRepository = userRepository;
        }

        public async Task<bool> LoginUserAsync(string username, string password, HttpContext httpContext)
        {
            var appUser = await AuthenticateUserAsync(username, password);

            if (appUser == null)
            {
                return false;
            }

            await appUser.SignInAsync(httpContext);

            return true;
        }

        public async Task<ClaimsIdentity> AuthenticateApiUserAsync(HttpContext context, string schemeName)
        {
            var credentials = GetBasicAuthCredentials(context).Split(':');

            if (credentials.Length != 2)
            {
                throw new AuthenticationException("Basic authentication string has a bad format");
            }

            var username = credentials[0];
            var password = credentials[1];

            var user = await AuthenticateUserAsync(username, password);

            if (user == null)
            {
                throw new AuthenticationException("Invalid username or password");
            }

            if (!user.Roles.Any(r => r.Role.RoleType == RoleType.Admin))
            {
                throw new AuthenticationException("Not enough permissions");
            }

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username),
            };

            return new ClaimsIdentity(claims, schemeName);
        }

        private async Task<Repository.Models.User> AuthenticateUserAsync(string username, string password)
        {
            _logger.LogInformation($"Trying to authenticate user '{username}'");

            await Task.CompletedTask;

            var appUser = _userRepository
                .Users
                .SingleOrDefault(u => u.Username.Equals(username));

            if (appUser == null)
            {
                _logger.LogInformation($"User with email '{username}' not found");
                return null;
            }

            if (!appUser.CheckPassword(password))
            {
                _logger.LogInformation($"The given password for user with username '{username}' does not match");
                return null;
            }

            _logger.LogInformation($"User with username '{username}' is authenticated");
            return appUser;
        }

        private string GetBasicAuthCredentials(HttpContext context)
        {
            if (context == null || context.Request == null)
            {
                throw new AuthenticationException("Invalid http context");
            }

            if (!context.Request.Headers.ContainsKey(AuthorizationHeader))
            {
                throw new AuthenticationException("Authentication header is missing");
            }

            var authHeader = AuthenticationHeaderValue.Parse(context.Request.Headers[AuthorizationHeader]);
            if (authHeader == null || authHeader.Parameter == null)
            {
                throw new AuthenticationException("Basic authentication string has a bad format");
            }

            var credentialBytes = Convert.FromBase64String(authHeader.Parameter);

            return Encoding.UTF8.GetString(credentialBytes);
        }
    }
}
