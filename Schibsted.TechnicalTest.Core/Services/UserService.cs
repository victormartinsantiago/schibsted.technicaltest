﻿namespace Schibsted.TechnicalTest.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Schibsted.TechnicalTest.Common.Exceptions;
    using Schibsted.TechnicalTest.Common.Model;
    using Schibsted.TechnicalTest.Core.Abstractions;
    using Schibsted.TechnicalTest.Repository.Abstractions;

    public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IUserRepository _userRepository;

        public UserService(
            IUserRepository userRepository,
            ILogger<UserService> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            _logger.LogInformation($"Fetching all users ...");

            await Task.CompletedTask;

            return _userRepository.Users.ToList().Select(u => new User
            {
                Id = u.Id,
                DisplayName = u.DisplayName,
                Username = u.Username,
                Roles = u.Roles.Select(r => r.Role.RoleType)
            });
        }

        public async Task<User> GetByIdAsync(Guid userId)
        {
            _logger.LogInformation($"Trying to fetch user with ID {userId}");

            var user = _userRepository.Users.SingleOrDefault(u => u.Id == userId);

            await Task.CompletedTask;

            if (user == null)
            {
                return null;
            }

            return new User
            {
                Id = user.Id,
                DisplayName = user.DisplayName,
                Username = user.Username,
                Roles = user.Roles.Select(r => r.Role.RoleType)
            };
        }

        public async Task<Guid> CreateAsync(User user)
        {
            _logger.LogInformation($"Creating new user {user.DisplayName}");

            if (user == null)
            {
                throw new BadRequestException("Invalid user object");
            }

            var repoUser = new Repository.Models.User
            {
                Id = Guid.NewGuid(),
                Username = user.Username,
                Salt = Guid.NewGuid(),
                DisplayName = user.DisplayName
            };

            repoUser.SetPassword(user.Password);
            user.Roles.ToList().ForEach(r => AddRole(repoUser, r));

            await _userRepository.Users.AddAsync(repoUser);
            await _userRepository.SaveChangesAsync();

            return repoUser.Id;
        }

        public async Task UpdateAsync(User user)
        {
            _logger.LogInformation($"Updating user {user}");

            if (user == null)
            {
                throw new BadRequestException("Invalid user object");
            }

            var repoUser = _userRepository.Users.SingleOrDefault(u => u.Id == user.Id);

            if (repoUser == null)
            {
                throw new NotFoundException($"User with ID {user.Id} could not be found");
            }

            repoUser.DisplayName = user.DisplayName;
            repoUser.SetPassword(user.Password);

            ClearUserRoles(repoUser);

            user.Roles.ToList().ForEach(r => AddRole(repoUser, r));

            await _userRepository.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid userId)
        {
            _logger.LogInformation($"Deleting user with ID {userId}");

            var user = _userRepository.Users.SingleOrDefault(u => u.Id == userId);

            if (user == null)
            {
                throw new NotFoundException($"User with ID {userId} could not be found");
            }

            _userRepository.Users.Remove(user);
            await _userRepository.SaveChangesAsync();
        }

        public async Task DeleteAllAsync()
        {
            _logger.LogInformation($"Deleting all users");

            _userRepository.Users.RemoveRange(_userRepository.Users);
            await _userRepository.SaveChangesAsync();
        }

        public async Task AddRoleAsync(Guid userId, IEnumerable<RoleType> roles)
        {
            _logger.LogInformation($"Adding roles to user with ID {userId}");

            var user = _userRepository.Users.SingleOrDefault(u => u.Id == userId);

            if (user == null)
            {
                throw new NotFoundException($"User with ID {userId} could not be found");
            }

            roles.ToList().ForEach(r => AddRole(user, r));

            await _userRepository.SaveChangesAsync();
        }

        public async Task RemoveRoleAsync(Guid userId, IEnumerable<RoleType> roles)
        {
            _logger.LogInformation($"Deleting roles from user with ID {userId}");

            var user = _userRepository.Users.SingleOrDefault(u => u.Id == userId);

            if (user == null)
            {
                throw new NotFoundException($"User with ID {userId} could not be found");
            }

            roles.ToList().ForEach(r => RemoveRole(user, r));

            await _userRepository.SaveChangesAsync();
        }

        private void AddRole(Repository.Models.User user, RoleType roleType)
        {
            _logger.LogInformation($"Adding role {roleType.ToString()} to user {user}");

            var role = _userRepository.Roles.Single(r => r.RoleType == roleType);

            if (role == null)
            {
                throw new NotFoundException($"Role of type {roleType.ToString()} could not be found");
            }

            if (user.Roles.Any(r => r.Role.RoleType == roleType))
            {
                throw new BadRequestException($"User with ID {user.Id} already has a permission of type {roleType.ToString()}");
            }

            user.Roles.Add(new Repository.Models.UserRole
            {
                UserId = user.Id,
                RoleId = role.Id,
                User = user,
                Role = role
            });
        }

        private void RemoveRole(Repository.Models.User user, RoleType roleType)
        {
            _logger.LogInformation($"Adding role {roleType.ToString()} to user {user}");

            var role = _userRepository.Roles.Single(r => r.RoleType == roleType);

            if (role == null)
            {
                throw new NotFoundException($"Role of type {roleType.ToString()} could not be found");
            }

            if (!user.Roles.Any(r => r.Role.RoleType == roleType))
            {
                throw new BadRequestException($"User with ID {user.Id} does not have a permission of type {roleType.ToString()}");
            }

            var userRole = user.Roles.Single(r => r.RoleId == role.Id && r.UserId == user.Id);

            user.Roles.Remove(userRole);
        }

        private void ClearUserRoles(Repository.Models.User user)
        {
            foreach (var role in user.Roles)
            {
                role.Role.Users.Remove(role);
            }

            user.Roles.Clear();
        }
    }
}
