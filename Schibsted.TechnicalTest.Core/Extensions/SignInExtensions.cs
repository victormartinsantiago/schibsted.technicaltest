﻿namespace Schibsted.TechnicalTest.Infrastructure.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Http;
    using Schibsted.TechnicalTest.Repository.Models;

    public static class SignInExtensions
    {
        public static async Task SignInAsync(this User appUser, HttpContext httpContext)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, appUser.Id.ToString()),
                new Claim(ClaimTypes.Name, appUser.DisplayName),
                new Claim(ClaimTypes.Email, appUser.Username)
            };

            appUser.Roles.ToList().ForEach(r => claims.Add(new Claim(ClaimTypes.Role, r.Role.RoleType.ToString())));

            var claimsIdentity = new ClaimsIdentity(
                claims,
                CookieAuthenticationDefaults.AuthenticationScheme);

            await httpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity));
        }

        public static async Task SignOutAsync(HttpContext httpContext)
        {
            await httpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}
