﻿namespace Schibsted.TechnicalTest.Infrastructure.Extensions
{
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.Extensions.DependencyInjection;
    using Schibsted.TechnicalTest.Infrastructure.Handlers;

    public static class AuthenticationExtensions
    {
        private const string SchemeName = "BasicAuthentication";

        public static void AddBasicAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(SchemeName)
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>(SchemeName, (options) => { });
        }
    }
}
