﻿namespace Schibsted.TechnicalTest.Infrastructure.Builders
{
    using Microsoft.AspNetCore.Authorization;
    using Schibsted.TechnicalTest.Common.Model;
    using Schibsted.TechnicalTest.Repository.Models;

    public static class PolicyBuilder
    {
        public static void Build(this AuthorizationOptions options)
        {
            options.AddPolicy(nameof(RoleType.Admin), policy => policy.RequireRole(nameof(RoleType.Admin)));
            options.AddPolicy(nameof(RoleType.Page1), policy => policy.RequireRole(nameof(RoleType.Page1), nameof(RoleType.Admin)));
            options.AddPolicy(nameof(RoleType.Page2), policy => policy.RequireRole(nameof(RoleType.Page2), nameof(RoleType.Admin)));
            options.AddPolicy(nameof(RoleType.Page3), policy => policy.RequireRole(nameof(RoleType.Page3), nameof(RoleType.Admin)));
        }
    }
}
