﻿namespace Schibsted.TechnicalTest.WebSite.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Schibsted.TechnicalTest.Common.Model;

    [Authorize(Policy = nameof(RoleType.Page1))]
    public class Page1Controller : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}
