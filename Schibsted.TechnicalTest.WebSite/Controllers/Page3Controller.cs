﻿namespace Schibsted.TechnicalTest.WebSite.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Schibsted.TechnicalTest.Common.Model;

    [Authorize(Policy = nameof(RoleType.Page3))]
    public class Page3Controller : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
