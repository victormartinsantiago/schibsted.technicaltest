﻿namespace Schibsted.TechnicalTest.WebSite.Controllers
{
    using System.Text.Encodings.Web;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Schibsted.TechnicalTest.Core.Abstractions;
    using Schibsted.TechnicalTest.Infrastructure.Extensions;
    using Schibsted.TechnicalTest.WebSite.Models;

    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly HtmlEncoder _encoder;
        private readonly ILogger<AccountController> _logger;
        private readonly IAuthenticationService _authenticationService;

        public AccountController(
            HtmlEncoder htmlEncoder,
            ILogger<AccountController> logger,
            IAuthenticationService authenticationService)
        {
            _logger = logger;
            _encoder = htmlEncoder;
            _authenticationService = authenticationService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Login(string returnUrl = null)
        {
            await SignInExtensions.SignOutAsync(HttpContext);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (await _authenticationService.LoginUserAsync(model.Username, model.Password, HttpContext))
            {
                return RedirectToLocal(returnUrl);
            }

            ModelState.AddModelError(string.Empty, "Please, review user and password combination");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Logout()
        {
            await SignInExtensions.SignOutAsync(HttpContext);

            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet]
        public ActionResult AccessDenied()
        {
            return View();
        }

        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction(nameof(HomeController.Index), nameof(HomeController));
        }
    }
}