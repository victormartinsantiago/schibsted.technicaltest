﻿namespace Schibsted.TechnicalTest.Core.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using NUnit.Framework;
    using Schibsted.TechnicalTest.Common.Model;
    using Schibsted.TechnicalTest.Core.Abstractions;
    using Schibsted.TechnicalTest.Core.Services;
    using Schibsted.TechnicalTest.Core.Tests.Collections;
    using Schibsted.TechnicalTest.Repository.Configurators;
    using Schibsted.TechnicalTest.Tests.Common;

    public class UserServiceTests : TestBase
    {
        private readonly UsersCollection _usersCollection = new UsersCollection();

        public override void AddConfiguration(IConfigurationBuilder configurationBuilder)
        {
        }

        public override void RegisterDependencies(IServiceCollection serviceCollection)
        {
            serviceCollection.AddLogging();
            serviceCollection.AddRepository(false);
            serviceCollection.AddTransient<IUserService, UserService>();
        }

        [TestCase(3, 1)]
        [TestCase(2, 2)]
        [TestCase(1, 3)]
        public async Task TestGetUsers(int userCount, int roleCount)
        {
            var users = await AddUsersAsync(userCount, roleCount);
            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var existingUsers = await userService.GetAll();

            Assert.IsNotNull(existingUsers, $"{nameof(existingUsers)} should not be null");
            Assert.AreEqual(users.Count(), existingUsers.Count(), $"The count of users in {nameof(users)} should match the number of users in {nameof(existingUsers)}");

            foreach (var existingUser in existingUsers)
            {
                var user = users.Single(u => u.Username == existingUser.Username);
                Assert.AreEqual(user.DisplayName, existingUser.DisplayName, $"{nameof(user.DisplayName)} should be equals to {nameof(existingUser.DisplayName)}");
                Assert.AreEqual(user.Username, existingUser.Username, $"{nameof(user.Username)} should be equals to {nameof(existingUser.Username)}");
                Assert.AreEqual(user.Roles.Count(), existingUser.Roles.Count(), $"Role count {user.Roles.Count()} should be equals to {existingUser.Roles.Count()}");
            }
        }

        [TestCase(3)]
        [TestCase(2)]
        [TestCase(1)]
        public async Task TestCreateUsers(int userCount)
        {
            var users = new List<User>();
            var userService = ServiceProvider.GetRequiredService<IUserService>();

            await userService.DeleteAllAsync();

            for (int i = 0; i < userCount; i++)
            {
                var user = new User
                {
                    DisplayName = $"TestUser {i}",
                    Username = $"Username {i}",
                    Password = $"Pasword{i}",
                    Roles = new List<RoleType>
                    { RoleType.Page1, RoleType.Page2 }
                };

                await userService.CreateAsync(user);

                users.Add(user);
            }

            var existingUsers = await userService.GetAll();

            Assert.IsNotNull(existingUsers, $"{nameof(existingUsers)} should not be null");
            Assert.AreEqual(users.Count(), existingUsers.Count(), $"The count of users in {nameof(users)} should match the number of users in {nameof(existingUsers)}");

            foreach (var existingUser in existingUsers)
            {
                var user = users.Single(u => u.Username == existingUser.Username);
                Assert.AreEqual(user.DisplayName, existingUser.DisplayName, $"{nameof(user.DisplayName)} should be equals to {nameof(existingUser.DisplayName)}");
                Assert.AreEqual(user.Username, existingUser.Username, $"{nameof(user.Username)} should be equals to {nameof(existingUser.Username)}");
                Assert.AreEqual(user.Roles.Count(), existingUser.Roles.Count(), $"Role count {user.Roles.Count()} should be equals to {existingUser.Roles.Count()}");
            }
        }

        [TestCase(3, 1)]
        [TestCase(2, 2)]
        [TestCase(1, 3)]
        public async Task TestUpdateUsers(int userCount, int roleCount)
        {
            var users = await AddUsersAsync(userCount, roleCount);
            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var existingUsers = await userService.GetAll();

            Assert.IsNotNull(existingUsers, $"{nameof(existingUsers)} should not be null");
            Assert.AreEqual(users.Count(), existingUsers.Count(), $"The count of users in {nameof(users)} should match the number of users in {nameof(existingUsers)}");

            foreach (var existingUser in existingUsers)
            {
                var user = users.Single(u => u.Username == existingUser.Username);
                Assert.AreEqual(user.DisplayName, existingUser.DisplayName, $"{nameof(user.DisplayName)} should be equals to {nameof(existingUser.DisplayName)}");
                Assert.AreEqual(user.Username, existingUser.Username, $"{nameof(user.Username)} should be equals to {nameof(existingUser.Username)}");
                Assert.AreEqual(user.Roles.Count(), existingUser.Roles.Count(), $"Role count {user.Roles.Count()} should be equals to {existingUser.Roles.Count()}");

                existingUser.DisplayName = $"Updated DisplayName {Guid.NewGuid()}";
                existingUser.Password = $"Updated Password {Guid.NewGuid()}";
                existingUser.Username = $"Updated username {Guid.NewGuid()}";

                var roles = new List<RoleType>
                {
                    RoleType.Page1,
                    RoleType.Page2,
                    RoleType.Page3
                };

                existingUser.Roles = roles;

                await userService.UpdateAsync(existingUser);
            }

            var updatedUsers = await userService.GetAll();

            foreach (var updatedUser in updatedUsers)
            {
                var user = existingUsers.Single(u => u.Username == updatedUser.Username);
                Assert.AreEqual(user.DisplayName, updatedUser.DisplayName, $"{nameof(user.DisplayName)} should be equals to {nameof(updatedUser.DisplayName)}");
                Assert.AreEqual(user.Username, updatedUser.Username, $"{nameof(user.Username)} should be equals to {nameof(updatedUser.Username)}");
                Assert.AreEqual(user.Roles.Count(), updatedUser.Roles.Count(), $"Role count {user.Roles.Count()} should be equals to {updatedUser.Roles.Count()}");
            }
        }

        [TestCase(3, 1)]
        [TestCase(2, 2)]
        [TestCase(1, 3)]
        public async Task TestDeleteUsers(int userCount, int roleCount)
        {
            var users = await AddUsersAsync(userCount, roleCount);
            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var existingUsers = await userService.GetAll();

            Assert.IsNotNull(existingUsers, $"{nameof(existingUsers)} should not be null");
            Assert.AreEqual(users.Count(), existingUsers.Count(), $"The count of users in {nameof(users)} should match the number of users in {nameof(existingUsers)}");

            foreach (var existingUser in existingUsers)
            {
                await userService.DeleteAsync(existingUser.Id);
            }

            existingUsers = await userService.GetAll();

            Assert.IsNotNull(existingUsers, $"{nameof(existingUsers)} should not be null");
            Assert.AreEqual(existingUsers.Count(), 0, $"There should be no users in {nameof(existingUsers)}");
        }

        [TestCase(3, 1)]
        [TestCase(2, 2)]
        [TestCase(1, 3)]
        public async Task TestDeleteAllUsers(int userCount, int roleCount)
        {
            var users = await AddUsersAsync(userCount, roleCount);
            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var existingUsers = await userService.GetAll();

            Assert.IsNotNull(existingUsers, $"{nameof(existingUsers)} should not be null");
            Assert.AreEqual(users.Count(), existingUsers.Count(), $"The count of users in {nameof(users)} should match the number of users in {nameof(existingUsers)}");

            await userService.DeleteAllAsync();

            existingUsers = await userService.GetAll();

            Assert.IsNotNull(existingUsers, $"{nameof(existingUsers)} should not be null");
            Assert.AreEqual(existingUsers.Count(), 0, $"There should be no users in {nameof(existingUsers)}");
        }

        [TestCase(RoleType.Page1)]
        [TestCase(RoleType.Page1, RoleType.Page2)]
        [TestCase(RoleType.Page1, RoleType.Page2, RoleType.Page3)]
        public async Task TestAddRoles(params RoleType[] roles)
        {
            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var user = new User
            {
                DisplayName = "Username",
                Password = "Password",
                Username = "Username"
            };
            var userId = await userService.CreateAsync(user);
            var existingUser = await userService.GetByIdAsync(userId);

            Assert.IsNotNull(existingUser, $"{nameof(existingUser)} should not be null");
            Assert.AreEqual(user.DisplayName, existingUser.DisplayName, $"{nameof(user.DisplayName)} should be equals to {nameof(existingUser.DisplayName)}");
            Assert.AreEqual(user.Username, existingUser.Username, $"{nameof(user.Username)} should be equals to {nameof(existingUser.Username)}");
            Assert.AreEqual(user.Roles.Count(), existingUser.Roles.Count(), $"Role count {user.Roles.Count()} should be equals to {existingUser.Roles.Count()}");

            await userService.AddRoleAsync(userId, roles);

            existingUser = await userService.GetByIdAsync(userId);

            Assert.AreEqual(existingUser.Roles.Count(), roles.Count(), $"Role count {user.Roles.Count()} should be equals to {roles.Count()}");
        }

        [TestCase(RoleType.Page1)]
        [TestCase(RoleType.Page1, RoleType.Page2)]
        [TestCase(RoleType.Page1, RoleType.Page2, RoleType.Page3)]
        public async Task TestRemoveRoles(params RoleType[] roles)
        {
            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var user = new User
            {
                DisplayName = "Username",
                Password = "Password",
                Username = "Username"
            };
            var userId = await userService.CreateAsync(user);
            var existingUser = await userService.GetByIdAsync(userId);

            Assert.IsNotNull(existingUser, $"{nameof(existingUser)} should not be null");
            Assert.AreEqual(user.DisplayName, existingUser.DisplayName, $"{nameof(user.DisplayName)} should be equals to {nameof(existingUser.DisplayName)}");
            Assert.AreEqual(user.Username, existingUser.Username, $"{nameof(user.Username)} should be equals to {nameof(existingUser.Username)}");
            Assert.AreEqual(user.Roles.Count(), existingUser.Roles.Count(), $"Role count {user.Roles.Count()} should be equals to {existingUser.Roles.Count()}");

            await userService.AddRoleAsync(userId, roles);

            existingUser = await userService.GetByIdAsync(userId);

            Assert.AreEqual(existingUser.Roles.Count(), roles.Count(), $"Role count {existingUser.Roles.Count()} should be equals to {roles.Count()}");

            await userService.RemoveRoleAsync(userId, roles);

            existingUser = await userService.GetByIdAsync(userId);

            Assert.AreEqual(existingUser.Roles.Count(), 0, $"{nameof(existingUser)} should have no roles");
        }

        private async Task<IEnumerable<User>> AddUsersAsync(int count, int roleCount = 1)
        {
            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var users = _usersCollection.Users
                .Where(u => u.Roles.Count() == roleCount)
                .Take(count)
                .ToList();

            await userService.DeleteAllAsync();
            users.ForEach(async u => await userService.CreateAsync(u));

            return users;
        }
    }
}
