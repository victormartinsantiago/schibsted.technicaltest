﻿namespace Schibsted.TechnicalTest.Core.Tests
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using NUnit.Framework;
    using Schibsted.TechnicalTest.Common.Model;
    using Schibsted.TechnicalTest.Core.Abstractions;
    using Schibsted.TechnicalTest.Core.Services;
    using Schibsted.TechnicalTest.Core.Tests.Collections;
    using Schibsted.TechnicalTest.Repository.Configurators;
    using Schibsted.TechnicalTest.Tests.Common;

    public class AuthenticationServiceTest : TestBase
    {
        private readonly UsersCollection _usersCollection = new UsersCollection();

        public override void AddConfiguration(IConfigurationBuilder configurationBuilder)
        {
        }

        public override void RegisterDependencies(IServiceCollection serviceCollection)
        {
            serviceCollection.AddLogging();
            serviceCollection.AddRepository(false);
            serviceCollection.AddTransient<IUserService, UserService>();
            serviceCollection.AddTransient<IAuthenticationService, AuthenticationService>();
        }

        [TestCase(UsersCollection.Username, "Wrong Password", false)]
        [TestCase("Wrongusername", UsersCollection.Password, false)]
        public async Task TestLoginUser(string username, string password, bool result)
        {
            var user = new User
            {
                DisplayName = "TestUser",
                Password = UsersCollection.Password,
                Username = UsersCollection.Username,
                Roles = new List<RoleType> { RoleType.Page1 }
            };

            var userService = ServiceProvider.GetRequiredService<IUserService>();
            var authenticationService = ServiceProvider.GetRequiredService<IAuthenticationService>();

            await userService.DeleteAllAsync();
            await userService.CreateAsync(user);

            using (var requestServices = ServiceProvider.CreateScope())
            {
                var httpContext = new DefaultHttpContext { RequestServices = requestServices.ServiceProvider };
                var authResult = await authenticationService.LoginUserAsync(username, password, new DefaultHttpContext());

                Assert.AreEqual(authResult, result, $"Expected result was {result} but got {authResult}");
            }
        }
    }
}
