﻿namespace Schibsted.TechnicalTest.Core.Tests.Collections
{
    using System.Collections.Generic;
    using Schibsted.TechnicalTest.Common.Model;

    public class UsersCollection
    {
        public const string Username = "Username";
        public const string Password = "Password";

        private readonly List<User> _users = new List<User>
        {
            new User
            {
                DisplayName = "User1",
                Username = "user1@schibsted.com",
                Password = "12345",
                Roles = new List<RoleType> { RoleType.Page1 }
            },
            new User
            {
                DisplayName = "User2",
                Username = "user2@schibsted.com",
                Password = "12345",
                Roles = new List<RoleType> { RoleType.Page2 }
            },
            new User
            {
                DisplayName = "User3",
                Username = "user3@schibsted.com",
                Password = "12345",
                Roles = new List<RoleType> { RoleType.Page3 }
            },
            new User
            {
                DisplayName = "User4",
                Username = "use4@schibsted.com",
                Password = "12345",
                Roles = new List<RoleType> { RoleType.Page1, RoleType.Page2 }
            },
            new User
            {
                DisplayName = "User5",
                Username = "user5@schibsted.com",
                Password = "12345",
                Roles = new List<RoleType> { RoleType.Page2, RoleType.Page3 }
            },
            new User
            {
                DisplayName = "User6",
                Username = "user6@schibsted.com",
                Password = "12345",
                Roles = new List<RoleType> { RoleType.Page1, RoleType.Page2, RoleType.Page3 }
            }
        };

        public IEnumerable<User> Users => _users;
    }
}
